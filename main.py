import random

def pick_a_num():

    while True:
        limit = int(input('Out of how many numbers you would like me to pick one? (type 0 to EXIT): '))

        if limit != 0:
            return (random.randint(1, limit), limit)
        else: 
            print('👋 Byeeee!')
            exit()

def guess(random_secret_num, limit):
    
    while True:
        usr_guess = int(input(f'Make a guess of a number between 1 and {limit} (type 0 to EXIT): '))
        
        if usr_guess == 0:
            print('👋 Byeeee!')
            exit()
        elif usr_guess < random_secret_num:
            print('It is too low. Try again!')
        elif usr_guess > random_secret_num:
            print('It is too much. Try again!')
        else:
            print(f'Yay! You have guessed it correctly! It is {random_secret_num}')
            break

if __name__ == '__main__':
    rand_num, limit = pick_a_num()
    guess(rand_num, limit)
