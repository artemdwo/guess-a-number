# Guess a number

"Guess a number" game. 

Objective: guess a number that the program picks out of a range. 

The range has an upper limit specified by user.

> NOTE: EXIT can be performed with 0 (zero)

## Run

`$ python main.py`

or

`$ python3 main.py`

Example
![example](./gif/example.gif)